import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { GatewayProvider } from '../providers/gateway/gateway';
import { GlobalProvider } from '../providers/global/global';
import { HttpModule } from '@angular/http';
import { StagePage } from '../pages/stage/stage';
import { TaskListPage } from '../pages/task-list/task-list';
import { TaskDetailsPage } from '../pages/task-details/task-details';
import { TaskPopoverPage } from '../pages/task-popover/task-popover';
import { TimeAgoPipe } from 'time-ago-pipe';
// import { FCM } from '@ionic-native/fcm'


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        LoginPage,
        StagePage,
        TaskListPage,
        TaskDetailsPage,
        TaskPopoverPage,
        TimeAgoPipe
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp,
            {
                mode: 'ios',
                menuType: 'overlay',
            }

        ),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        LoginPage,
        StagePage,
        TaskListPage,
        TaskDetailsPage,
        TaskPopoverPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        GatewayProvider,
        GlobalProvider,
        // FCM
    ]
})
export class AppModule { }
