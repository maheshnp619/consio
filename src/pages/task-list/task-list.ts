import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TaskDetailsPage } from '../task-details/task-details';
import { GlobalProvider } from '../../providers/global/global';
import { GatewayProvider } from '../../providers/gateway/gateway';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { TaskPopoverPage } from '../task-popover/task-popover';

@IonicPage()
@Component({
    selector: 'page-task-list',
    templateUrl: 'task-list.html',
})
export class TaskListPage {
    tasks = [];
    breadcrumDetail = [];
    searchTerm: string = "";
    constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public gateway: GatewayProvider, public viewCtrl: ViewController, public navParams: NavParams, public global: GlobalProvider) {
        console.log("breadcrumDetail", this.navParams.get("breadcrumDetail"));
        this.breadcrumDetail = this.navParams.get("breadcrumDetail");
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TaskListPage');
        this.viewCtrl.setBackButtonText('');
    }

    ionViewWillEnter() {
        this.viewCtrl.setBackButtonText('');
    }

    ionViewDidEnter() {
        this.getAllTasks();
    }



    taskDetails(task) {
        this.navCtrl.push(TaskDetailsPage, { data: task });
    }

    getAllTasks() {
        let self = this;
        let taskData = this.global.taskData;
        self.gateway.getAllTaskDetails(taskData).then((result: any) => {
            console.log("taskData", result);
            if (result.length > 0) {
                this.tasks = result.filter((par, index) => {
                    return index === result.findIndex((obj) => {
                        return JSON.stringify(obj) === JSON.stringify(par);
                    });
                });
                this.global.allTasks = this.tasks;
            }
        });
    }

    setFilteredItems() {
        this.tasks = this.global.filterItems(this.searchTerm);
    }

    sortTasks(ev) {
        let popover = this.popoverCtrl.create(TaskPopoverPage);
        popover.present({
            ev: ev
        });

        popover.onDidDismiss(data => {
            console.log("this.tasks", this.global.filterData(data));
            this.tasks = this.global.filterData(data);
        });
    }






}
