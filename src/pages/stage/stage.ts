import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GatewayProvider } from '../../providers/gateway/gateway';
import { TaskListPage } from '../task-list/task-list';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { GlobalProvider } from '../../providers/global/global';


@IonicPage()
@Component({
    selector: 'page-stage',
    templateUrl: 'stage.html',
})
export class StagePage {
    projectDetails: any = null;
    processName = "";
    stageName = "";
    activityName = "";
    activity = [];
    stages = [];
    processes = [];
    taskDetail = [];
    dupProcesses = [];
    dupActivities = [];
    activities = [];
    projDetails = {
        project_name: "",
        project_stage: "",
        project_process: "",
        project_activity: ""
    };
    constructor(public navCtrl: NavController, public global: GlobalProvider, public alertCtrl: AlertController, public navParams: NavParams, public viewCtrl: ViewController, public gateway: GatewayProvider) {
        this.projectDetails = this.navParams.get("projectDetail");
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad StagePage');
    }

    ionViewDidEnter() {
        this.processDetails();
    }

    ionViewWillEnter() {
        this.viewCtrl.setBackButtonText('');
    }

    getTasks() {
        this.taskDetail = this.activity.filter((data) => { return data.root_activity_id == this.activityName; });
    }

    processDetails() {
        let self = this;
        if (self.projectDetails !== undefined) {
            self.gateway.getStageDetails(self.projectDetails.project_id).then((result: any) => {
                console.log("result", result);
                if (result.success) {
                    self.stages = result.stages;
                    self.processes = result.processes;
                    self.activities = result.latest_activities;
                    self.dupProcesses = self.processes;
                    self.dupActivities = self.activities;
                    console.log("self.stages", self.stages);
                    console.log("self.processes", self.processes);
                    console.log("self.activities", self.dupActivities);
                }
            });
        }
    };

    changeActivity = function ($event) {
        console.log("$event", $event);
        this.activityName = "";
        this.activities = this.dupActivities.filter((t) => { return t.project_stage_id == this.stageName && t.project_stage_process_id == this.processName; });
        console.log("this.activities", this.activities);
        this.activity = [];
        if (this.activities.length > 0 && this.activity.length == 0) {
            for (let i = 0; i < this.activities.length; i++) {
                let act = this.activities[i];
                let activityObj = {
                    name: "",
                    project_activity_id: 0,
                    child_activity_id: "",
                    root_activity_id: "",
                    process_selected_id: 0,
                };
                if (act.activity_type == "parallel") {
                    for (let j = 0; j < act.activities.length; j++) {
                        let act_1 = act.activities[j];
                        activityObj.name = act_1.name;
                        activityObj.project_activity_id = act.project_activity_id;
                        activityObj.child_activity_id = act_1._id,
                            activityObj.root_activity_id = act._id,
                            activityObj.process_selected_id = act.project_stage_process_id;
                        console.log(activityObj, "activityObj");
                        this.activity.push(activityObj);
                    }
                }
                else if (act.activity_type == "rhombus") {
                    for (let k = 0; k < act.activities.length; k++) {
                        let act_1 = act.activities[k];
                        activityObj.name = act_1.name;
                        activityObj.project_activity_id = act.project_activity_id;
                        activityObj.child_activity_id = act_1._id,
                            activityObj.root_activity_id = act._id,
                            activityObj.process_selected_id = act.project_stage_process_id;
                        console.log(activityObj, "activityObj");
                        this.activity.push(activityObj);
                    }
                }
                else {
                    activityObj.name = act.activities.name;
                    activityObj.project_activity_id = act.project_activity_id;
                    activityObj.child_activity_id = act.activities._id,
                        activityObj.root_activity_id = act._id,
                        activityObj.process_selected_id = act.project_stage_process_id;
                    this.activity.push(activityObj);
                    console.log(activityObj, "activityObj");
                    console.log(this.activity, "activity");
                }
            }
            console.log(this.activity, "activity");
        }
        else {
            this.activity = [];
        }
    };

    changeProcess() {
        this.processName = "";
        let stageDetail = this.stages.filter((data) => { return data.id == this.stageName });
        this.projDetails.project_stage = stageDetail[0].stage_name;
        this.processes = this.dupProcesses.filter((data) => { return data.project_stage_id == this.stageName });
    };

    getAllTasks() {
        let self = this;
        if (self.checkValidation()) {
            let taskData = {
                project_id: self.projectDetails.project_id,
                project_stage_id: self.stageName,
                project_activity_id: self.taskDetail[0].project_activity_id,
                root_activity_id: self.taskDetail[0].root_activity_id,
                child_activity_id: self.taskDetail[0].child_activity_id,
                process_selected_id: self.taskDetail[0].process_selected_id
            };
            self.gateway.getAllTaskDetails(taskData).then((result: any) => {
                let procDetail = self.processes.filter((procData) => { return procData.id == self.processName; });
                let actData = self.activity.filter((actvityData) => { return actvityData.root_activity_id == self.activityName; });
                console.log("actData", actData);
                console.log("procDetail", procDetail);
                self.projDetails.project_name = self.projectDetails.project.name;
                self.projDetails.project_process = procDetail[0].process_name;
                self.projDetails.project_activity = actData[0].name;
                this.global.taskData = taskData;

                self.navCtrl.push(TaskListPage, { data: result, breadcrumDetail: self.projDetails });
            });
        }
    }

    checkValidation() {
        if (this.stageName == "" || this.stageName == undefined) {
            this.commonAlertCtrl("Please select stage");
            return false;
        }
        else if (this.processName == "" || this.processName == "") {
            this.commonAlertCtrl("Please select process");
            return false;
        }
        else if (this.activityName == "" || this.activityName == "") {
            this.commonAlertCtrl("Please select activity");
            return false;
        }
        else {
            return true;
        }
    }

    commonAlertCtrl(message) {
        let alert = this.alertCtrl.create({
            title: 'Error',
            message: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };





}
