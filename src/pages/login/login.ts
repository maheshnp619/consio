import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { GatewayProvider } from '../../providers/gateway/gateway';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    userName: string = "sumit.webwerks@gmail.com";
    passWord: string = "12345678";
    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public gateway: GatewayProvider) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    doLogin() {
        let self = this;
        if (self.checkValidation()) {
            let userdata = {
                email: this.userName,
                password: this.passWord,
                token: ""
            };
            self.gateway.login(userdata).then((result: any) => {
                console.log(result);
                if (result.success) {
                    window.localStorage.setItem("token", result.token);
                    self.navCtrl.setRoot(HomePage);
                }
                else {
                    self.commonAlertCtrl("Please enter correct username and password");
                }
            }).catch(function (error) {
                self.commonAlertCtrl("Please enter correct username and password");
            });
        }
    }

    checkValidation() {
        if (this.userName == "") {
            this.commonAlertCtrl("Please enter email id");
            return false;
        }
        else if (this.passWord == "") {
            this.commonAlertCtrl("Plese enter password");
            return false;
        }
        else {
            return true;
        }
    }

    commonAlertCtrl(message) {
        var alert = this.alertCtrl.create({
            title: 'Error',
            message: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };

}
