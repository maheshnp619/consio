import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GatewayProvider } from '../../providers/gateway/gateway';
import { StagePage } from '../stage/stage';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    projects: any = [];
    constructor(public navCtrl: NavController, public gateway: GatewayProvider) {

    }

    ionViewDidEnter() {
        let self = this;
        this.gateway.getDashboard().then((result: any) => {
            if (result.success) {
                self.projects = result.projects;
                console.log("self.projects", self.projects);
            }
        });
    };


    goToStages(projectDetail) {
        let self = this;
        self.navCtrl.push(StagePage, {
            projectDetail: projectDetail
        });
    };

}
