import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskDetailsPage } from './task-details';
import { TimeAgoPipe } from 'time-ago-pipe'

@NgModule({
    declarations: [
        TaskDetailsPage,
        TimeAgoPipe
    ],
    imports: [
        IonicPageModule.forChild(TaskDetailsPage),
    ],
})
export class TaskDetailsPageModule { }
