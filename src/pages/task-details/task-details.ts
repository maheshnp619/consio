import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, PopoverController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { GatewayProvider } from '../../providers/gateway/gateway';
import { TaskPopoverPage } from '../task-popover/task-popover';
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-task-details',
    templateUrl: 'task-details.html',
})
export class TaskDetailsPage {
    taskSeg: any = "people";
    task: any = null;
    statusName: any = "";
    tasks: any = [];
    message: any = "";
    commentData: any = []
    constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public navParams: NavParams, public global: GlobalProvider, public viewCtrl: ViewController,
        public gateway: GatewayProvider
    ) {
        this.task = this.navParams.get("data");
        if (this.task !== undefined) {
            this.statusName = this.task.status;
        }
        console.log(this.navParams.get("data"), "this.taskDetail");
    }

    ionViewDidEnter() {
        this.task = this.navParams.get("data");
        if (this.task !== undefined) {
            this.statusName = this.task.status;
            console.log(this.statusName, "this.statusName");
        }

        this.getAllViewComment();

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TaskDetailsPage');

    }

    approvedStatus() {
        let self = this;
        let apprvedData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: "Complete",
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id,
            creatorChoice: "Approved"
        }
        self.gateway.updateCreatorStatus(apprvedData).then((result: any) => {
            if (result.success) {
                console.log("creatorstatus", result);
                self.getAllTasks();
            }
        })

    }

    rejectedStatus() {
        let self = this;
        let rejectedData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: "Complete",
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id,
            creatorChoice: "Rejected"
        }
        self.gateway.updateCreatorStatus(rejectedData).then((result: any) => {
            if (result.success) {
                console.log("creatorstatus", result);
                self.getAllTasks();
                this.statusName = "In-progress";
            }
        })
    }

    changeStatus() {

    }

    updateTaskStatus() {
        let self = this;
        let requestData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: self.statusName,
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id
        }

        self.gateway.updateTaskStatus(requestData).then((result: any) => {
            if (result.success) {
                this.getAllTasks();
            }
        })
    }

    ionViewWillEnter() {
        this.viewCtrl.setBackButtonText('');
    }

    commentViewApi() {
        let self = this;
        let commentData = {
            commentable_type: "task",
            commentable_id: this.task._id
        }

        self.gateway.commentViewApi(commentData).then((result: any) => {
            if (result.success) {
                console.log("commentViewApi", result);
                self.getAllTasks();
            }
        });
    }

    getAllTasks() {
        let self = this;
        let taskData = this.global.taskData;
        self.gateway.getAllTaskDetails(taskData).then((result: any) => {
            console.log("taskData", result);
            if (result.length > 0) {
                self.tasks = result.filter((par, index) => {
                    return index === result.findIndex((obj) => {
                        return JSON.stringify(obj) === JSON.stringify(par);
                    });
                });
                let taskData = self.tasks.filter((procData) => { return procData._id == self.task._id });
                this.task = taskData[0];
                console.log(this.task, "this.task");
            }
        });
    }

    getAllViewComment() {
        let self = this;
        let commentViewData = {
            commentable_type: "task",
            commentable_id: self.task.id
        }
        self.gateway.commentViewApi(commentViewData).then((result: any) => {
            console.log("commentViewApi", result);
            this.commentData = this.sortData(result);
        });
    }

    sortData(result) {
        console.log("asdasd", result);
        return result.sort((a, b) => {
            if (a.createdAt > b.createdAt) {
                return 1;
            } else if (a.createdAt < b.createdAt) {
                return -1;
            } else {
                return 0;
            }
        });;
    }

    sendMessage() {
        let self = this;
        if (self.message !== "") {
            let messageData = {
                commentable_type: "task",
                commentable_id: self.task._id,
                comment_message: self.message,
                process_id: self.task.project_stage_process_id,
                task_id: self.task._id,
                activiy_id: self.task.project_activity_id,
                stage_id: self.task.project_stage_id,
                project_id: self.task.project_id
            }
            self.gateway.sendMessage(messageData).then((result: any) => {
                self.message = "";
                self.getAllViewComment();
            });
        }


    }

    calcuateDif(date) {
        let diff = Math.abs(new Date().getTime() - new Date(date).getTime());
        let diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        return diffDays;
    }


}
