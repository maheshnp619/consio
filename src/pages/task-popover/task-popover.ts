import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-task-popover',
    templateUrl: 'task-popover.html',
})
export class TaskPopoverPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TaskPopoverPage');
    }

    close(data) {
        this.viewCtrl.dismiss(data);
    }

}
