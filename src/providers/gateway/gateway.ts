import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { GlobalProvider } from '../global/global';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import 'rxjs/add/operator/map'


@Injectable()
export class GatewayProvider {
    loading: any = null;
    constructor(public http: Http, public global: GlobalProvider, public loadingCtrl: LoadingController) {
        console.log('Hello GatewayProvider Provider');
    }

    httpPost(reqBody, reqUrl) {
        let self = this;
        return new Promise(function (resolve, reject) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            self.showLoading();
            self.http
                .post(reqUrl, reqBody, { headers: headers })
                .map(res => { return res.json(); })
                .subscribe(response => {
                    self.hideLoading();
                    resolve(response);
                });
        }).catch(error => {
            console.log(error);
        });
    };


    getDashboard() {
        let self = this;
        return new Promise(function (resolve, reject) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .get(self.global.baseUrl + "dashboard", { headers: headers })
                .map((res) => { return res.json(); })
                .subscribe((response) => {
                    self.hideLoading();
                    resolve(response);
                });
        });
    };


    hideLoading() {
        if (this.loading) {
            this.loading.dismiss();
        }
    }



    login(userData) {
        let self = this;
        let url = self.global.baseUrl + "authenticate";
        return new Promise(function (resolve, reject) {
            self
                .httpPost(userData, url)
                .then((data: any) => {
                    resolve(data);
                })
                .catch(error => {
                    console.log(error);
                });
        });
    }


    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: '',
        });
        this.loading.present();
    }

    getStageDetails(projectId) {
        let self = this;
        return new Promise(function (resolve, reject) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .get(self.global.baseUrl + "projects/" + projectId + "/details", { headers: headers })
                .map((res) => { return res.json(); })
                .subscribe((response) => {
                    self.hideLoading();
                    resolve(response);
                });
        });
    };

    getAllTaskDetails(reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonPostApi("tasks/", reqData).then(function (result) {
                resolve(result);
            });
        });
    }

    commonPostApi(url, body) {
        let self = this;
        return new Promise(function (resolve, reject) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            self.showLoading();
            self.http
                .post(self.global.baseUrl + url, body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                    self.hideLoading();
                    resolve(response);
                });
        });
    }

    commonTokenPostApi(url, body) {
        let self = this;
        return new Promise(function (resolve, reject) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .post(self.global.baseUrl + url, body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                    self.hideLoading();
                    resolve(response);
                });
        });
    }


    updateTaskStatus(reqData) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("tasks/updateUserTaskStatus/", reqData).then(function (result) {
                resolve(result);
            });
        });
    }

    commentViewApi(reqData) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("comment/view", reqData).then(function (result) {
                resolve(result);
            });
        });
    }


    updateCreatorStatus(reqData) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("tasks/updateCreatorStatus", reqData).then(function (result) {
                resolve(result);
            });
        });

    }

    sendMessage(reqData) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("comment/store", reqData).then(function (result) {
                resolve(result);
            });
        });
    }

}
