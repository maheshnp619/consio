import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GlobalProvider {
    isDev = false;
    isLive = true;
    baseUrl = this.getBaseURl();
    token = "";
    taskData = {};
    allTasks = [];
    liveImgUrl: string = "https://s3-ap-northeast-1.amazonaws.com/consio-user-profile-images";
    defaultImgUrl: string = "https://consio.app/assets/images/profil_page/frienddefault.jpg";
    constructor(public http: Http) {
        console.log('Hello GlobalProvider Provider');
    }

    getBaseURl() {
        if (this.isDev) {
            return "http://localhost:3443/oxelus/api/v1/";
        }
        else {
            return "https://consio.app:3443/oxelus/api/v1/";
        }
    }

    filterItems(searchTerm) {
        if (this.allTasks.length > 0) {
            return this.allTasks.filter(item => {
                if (item.subject.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                } else if (item.status.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                } else if (item.creator_status !== undefined) {
                    if (item.creator_status.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                        return true;
                    }
                }
            });
        }
    }

    filterData(stringData) {
        console.log(stringData);
        console.log("alltasks", this.allTasks);

        return this.allTasks.sort((a, b) => {
            if (a[stringData] < b[stringData]) {
                return -1;
            } else if (a[stringData] > b[stringData]) {
                return 1;
            } else {
                return 0;
            }
        });
    }



}
