webpackJsonp([5],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_alert_alert_controller__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_gateway_gateway__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, alertCtrl, gateway) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.gateway = gateway;
        this.userName = "sumit.webwerks@gmail.com";
        this.passWord = "12345678";
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.doLogin = function () {
        var self = this;
        if (self.checkValidation()) {
            var userdata = {
                email: this.userName,
                password: this.passWord,
                token: ""
            };
            self.gateway.login(userdata).then(function (result) {
                console.log(result);
                if (result.success) {
                    window.localStorage.setItem("token", result.token);
                    self.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                }
                else {
                    self.commonAlertCtrl("Please enter correct username and password");
                }
            }).catch(function (error) {
                self.commonAlertCtrl("Please enter correct username and password");
            });
        }
    };
    LoginPage.prototype.checkValidation = function () {
        if (this.userName == "") {
            this.commonAlertCtrl("Please enter email id");
            return false;
        }
        else if (this.passWord == "") {
            this.commonAlertCtrl("Plese enter password");
            return false;
        }
        else {
            return true;
        }
    };
    LoginPage.prototype.commonAlertCtrl = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Error',
            message: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    ;
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/login/login.html"*/'<ion-content padding>\n    <ion-grid class="login-content">\n        <ion-row>\n            <ion-col text-center>\n                <div>\n                    <img src="assets/imgs/logo.png" style="height:65px;">\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div>\n                    <ion-item>\n                        <ion-input type="email" placeholder="Email" [(ngModel)]="userName" required></ion-input>\n                    </ion-item>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div>\n                    <ion-item>\n                        <ion-input type="password" placeholder="Password" [(ngModel)]="passWord"></ion-input>\n                    </ion-item>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row class="login-btn">\n            <ion-col col-4>\n                <button ion-button (click)="doLogin()" color="primary">\n                    Login\n                </button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_alert_alert_controller__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_gateway_gateway__["a" /* GatewayProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_gateway_gateway__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__task_list_task_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_navigation_view_controller__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StagePage = /** @class */ (function () {
    function StagePage(navCtrl, global, alertCtrl, navParams, viewCtrl, gateway) {
        this.navCtrl = navCtrl;
        this.global = global;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.gateway = gateway;
        this.projectDetails = null;
        this.processName = "";
        this.stageName = "";
        this.activityName = "";
        this.activity = [];
        this.stages = [];
        this.processes = [];
        this.taskDetail = [];
        this.dupProcesses = [];
        this.dupActivities = [];
        this.activities = [];
        this.projDetails = {
            project_name: "",
            project_stage: "",
            project_process: "",
            project_activity: ""
        };
        this.changeActivity = function ($event) {
            var _this = this;
            console.log("$event", $event);
            this.activityName = "";
            this.activities = this.dupActivities.filter(function (t) { return t.project_stage_id == _this.stageName && t.project_stage_process_id == _this.processName; });
            console.log("this.activities", this.activities);
            this.activity = [];
            if (this.activities.length > 0 && this.activity.length == 0) {
                for (var i = 0; i < this.activities.length; i++) {
                    var act = this.activities[i];
                    var activityObj = {
                        name: "",
                        project_activity_id: 0,
                        child_activity_id: "",
                        root_activity_id: "",
                        process_selected_id: 0,
                    };
                    if (act.activity_type == "parallel") {
                        for (var j = 0; j < act.activities.length; j++) {
                            var act_1 = act.activities[j];
                            activityObj.name = act_1.name;
                            activityObj.project_activity_id = act.project_activity_id;
                            activityObj.child_activity_id = act_1._id,
                                activityObj.root_activity_id = act._id,
                                activityObj.process_selected_id = act.project_stage_process_id;
                            console.log(activityObj, "activityObj");
                            this.activity.push(activityObj);
                        }
                    }
                    else if (act.activity_type == "rhombus") {
                        for (var k = 0; k < act.activities.length; k++) {
                            var act_1 = act.activities[k];
                            activityObj.name = act_1.name;
                            activityObj.project_activity_id = act.project_activity_id;
                            activityObj.child_activity_id = act_1._id,
                                activityObj.root_activity_id = act._id,
                                activityObj.process_selected_id = act.project_stage_process_id;
                            console.log(activityObj, "activityObj");
                            this.activity.push(activityObj);
                        }
                    }
                    else {
                        activityObj.name = act.activities.name;
                        activityObj.project_activity_id = act.project_activity_id;
                        activityObj.child_activity_id = act.activities._id,
                            activityObj.root_activity_id = act._id,
                            activityObj.process_selected_id = act.project_stage_process_id;
                        this.activity.push(activityObj);
                        console.log(activityObj, "activityObj");
                        console.log(this.activity, "activity");
                    }
                }
                console.log(this.activity, "activity");
            }
            else {
                this.activity = [];
            }
        };
        this.projectDetails = this.navParams.get("projectDetail");
    }
    StagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StagePage');
    };
    StagePage.prototype.ionViewDidEnter = function () {
        this.processDetails();
    };
    StagePage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.setBackButtonText('');
    };
    StagePage.prototype.getTasks = function () {
        var _this = this;
        this.taskDetail = this.activity.filter(function (data) { return data.root_activity_id == _this.activityName; });
    };
    StagePage.prototype.processDetails = function () {
        var self = this;
        if (self.projectDetails !== undefined) {
            self.gateway.getStageDetails(self.projectDetails.project_id).then(function (result) {
                console.log("result", result);
                if (result.success) {
                    self.stages = result.stages;
                    self.processes = result.processes;
                    self.activities = result.latest_activities;
                    self.dupProcesses = self.processes;
                    self.dupActivities = self.activities;
                    console.log("self.stages", self.stages);
                    console.log("self.processes", self.processes);
                    console.log("self.activities", self.dupActivities);
                }
            });
        }
    };
    ;
    StagePage.prototype.changeProcess = function () {
        var _this = this;
        this.processName = "";
        var stageDetail = this.stages.filter(function (data) { return data.id == _this.stageName; });
        this.projDetails.project_stage = stageDetail[0].stage_name;
        this.processes = this.dupProcesses.filter(function (data) { return data.project_stage_id == _this.stageName; });
    };
    ;
    StagePage.prototype.getAllTasks = function () {
        var _this = this;
        var self = this;
        if (self.checkValidation()) {
            var taskData_1 = {
                project_id: self.projectDetails.project_id,
                project_stage_id: self.stageName,
                project_activity_id: self.taskDetail[0].project_activity_id,
                root_activity_id: self.taskDetail[0].root_activity_id,
                child_activity_id: self.taskDetail[0].child_activity_id,
                process_selected_id: self.taskDetail[0].process_selected_id
            };
            self.gateway.getAllTaskDetails(taskData_1).then(function (result) {
                var procDetail = self.processes.filter(function (procData) { return procData.id == self.processName; });
                var actData = self.activity.filter(function (actvityData) { return actvityData.root_activity_id == self.activityName; });
                console.log("actData", actData);
                console.log("procDetail", procDetail);
                self.projDetails.project_name = self.projectDetails.project.name;
                self.projDetails.project_process = procDetail[0].process_name;
                self.projDetails.project_activity = actData[0].name;
                _this.global.taskData = taskData_1;
                self.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__task_list_task_list__["a" /* TaskListPage */], { data: result, breadcrumDetail: self.projDetails });
            });
        }
    };
    StagePage.prototype.checkValidation = function () {
        if (this.stageName == "" || this.stageName == undefined) {
            this.commonAlertCtrl("Please select stage");
            return false;
        }
        else if (this.processName == "" || this.processName == "") {
            this.commonAlertCtrl("Please select process");
            return false;
        }
        else if (this.activityName == "" || this.activityName == "") {
            this.commonAlertCtrl("Please select activity");
            return false;
        }
        else {
            return true;
        }
    };
    StagePage.prototype.commonAlertCtrl = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Error',
            message: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    ;
    StagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-stage',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/stage/stage.html"*/'<ion-header text-center>\n    <ion-navbar>\n        <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>\n            <img src="assets/imgs/logo.png" height="38">\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <div class="headerTitle">\n        {{projectDetails.project.project_code}} - {{projectDetails.project.name}}\n    </div>\n    <div>\n        <ion-grid style="margin-top:12%;">\n            <ion-row class="stagesList">\n                <ion-col>\n                    <div class="dropdwnTitle">\n                        Stages\n                    </div>\n                    <ion-item>\n                        <ion-label>Stages</ion-label>\n                        <ion-select multiple="false" [(ngModel)]="stageName" mulitple="false" (ionChange)="changeProcess()" style="width:100%; max-width:100%">\n                            <ion-option value="">Please select stage</ion-option>\n                            <ion-option *ngFor="let stage of stages" [value]="stage.id">{{stage.stage_name}}</ion-option>\n                        </ion-select>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            <ion-row class="stagesList">\n                <ion-col>\n                    <div class="dropdwnTitle">\n                        Processes\n                    </div>\n                    <ion-item>\n                        <ion-label>Processes</ion-label>\n                        <ion-select [(ngModel)]="processName" mulitple="false" (ionChange)="changeActivity()" style="width:100%; max-width:100%">\n                            <ion-option value="">Please select process</ion-option>\n                            <ion-option *ngFor="let process of processes" [value]="process.id">{{process.process_name}}</ion-option>\n                        </ion-select>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            <ion-row class="stagesList">\n                <ion-col>\n                    <div class="dropdwnTitle">\n                        Activities\n                    </div>\n                    <ion-item>\n                        <ion-label>Activities</ion-label>\n                        <ion-select [(ngModel)]="activityName" mulitple="false" (ionChange)="getTasks()" style="width:100%; max-width:100%">\n                            <ion-option value="">Please select activity</ion-option>\n                            <ion-option *ngFor="let act of activity" [value]="act.root_activity_id">{{act.name}}</ion-option>\n                        </ion-select>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col text-center>\n                    <button ion-button class="taskBtn" (click)="getAllTasks()">\n                        Submit\n                    </button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n\n    <!-- </ion-list> -->\n</ion-content>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/stage/stage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular_navigation_view_controller__["a" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_gateway_gateway__["a" /* GatewayProvider */]])
    ], StagePage);
    return StagePage;
}());

//# sourceMappingURL=stage.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__task_details_task_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_global__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_gateway_gateway__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_popover_popover_controller__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__task_popover_task_popover__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TaskListPage = /** @class */ (function () {
    function TaskListPage(navCtrl, popoverCtrl, gateway, viewCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.gateway = gateway;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.global = global;
        this.tasks = [];
        this.breadcrumDetail = [];
        this.searchTerm = "";
        console.log("breadcrumDetail", this.navParams.get("breadcrumDetail"));
        this.breadcrumDetail = this.navParams.get("breadcrumDetail");
    }
    TaskListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TaskListPage');
        this.viewCtrl.setBackButtonText('');
    };
    TaskListPage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.setBackButtonText('');
    };
    TaskListPage.prototype.ionViewDidEnter = function () {
        this.getAllTasks();
    };
    TaskListPage.prototype.taskDetails = function (task) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__task_details_task_details__["a" /* TaskDetailsPage */], { data: task });
    };
    TaskListPage.prototype.getAllTasks = function () {
        var _this = this;
        var self = this;
        var taskData = this.global.taskData;
        self.gateway.getAllTaskDetails(taskData).then(function (result) {
            console.log("taskData", result);
            if (result.length > 0) {
                _this.tasks = result.filter(function (par, index) {
                    return index === result.findIndex(function (obj) {
                        return JSON.stringify(obj) === JSON.stringify(par);
                    });
                });
                _this.global.allTasks = _this.tasks;
            }
        });
    };
    TaskListPage.prototype.setFilteredItems = function () {
        this.tasks = this.global.filterItems(this.searchTerm);
    };
    TaskListPage.prototype.sortTasks = function (ev) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_6__task_popover_task_popover__["a" /* TaskPopoverPage */]);
        popover.present({
            ev: ev
        });
        popover.onDidDismiss(function (data) {
            console.log("this.tasks", _this.global.filterData(data));
            _this.tasks = _this.global.filterData(data);
        });
    };
    TaskListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-task-list',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/task-list/task-list.html"*/'<ion-header text-center>\n    <ion-navbar>\n        <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>\n            <img src="assets/imgs/logo.png" height="38">\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <div *ngIf="breadcrumDetail !== undefined" class="breadcrumDetails">\n        <span>\n            {{ breadcrumDetail.project_name }}\n            <ion-icon name="arrow-forward"></ion-icon> {{ breadcrumDetail.project_stage }}\n            <ion-icon name="arrow-forward"></ion-icon> {{ breadcrumDetail.project_process }}\n            <ion-icon name="arrow-forward"></ion-icon> {{ breadcrumDetail.project_activity}}\n        </span>\n        <div class="searchBar">\n            <span>\n                <ion-searchbar [(ngModel)]="searchTerm" (ionChange)="setFilteredItems()"></ion-searchbar>\n            </span>\n            <span class="funnelIcon" (click)="sortTasks($event)">\n                <ion-icon name="funnel"></ion-icon>\n            </span>\n\n        </div>\n        <div>\n\n        </div>\n    </div>\n    <ion-card *ngFor="let task of tasks" (click)="taskDetails(task)">\n        <div *ngIf="tasks.length > 0">\n            <ion-card-header class="taskSubject">\n                <span class="uniqueTask">{{ task.task_unique_no }}</span> {{task.subject}}\n            </ion-card-header>\n            <ion-card-content>\n                <div class="taskContent">\n                    <div class="dueDate" [ngClass]="{\'low-class\': task.priority ==\'low\', \'high-class\': task.priority ==\'high\', \'medium-class\': task.priority== \'medium\' }">\n\n                        {{task.due_date | date: \'d MMMM y\'}}\n                    </div>\n                    <div *ngIf="task.status == \'Complete\'">\n                        <div class="taskStatus" *ngIf="task.creator_status == undefined">\n                            {{task.status}} / Waiting Approval\n                        </div>\n                        <div class="taskStatus" *ngIf="task.creator_status == \'Approved\'">\n                            {{task.status}} / {{task.creator_status}}\n                        </div>\n                        <div class="taskStatus" *ngIf="task.creator_status == \'Rejected\'">\n                            {{task.status}} / {{task.creator_status}}\n                        </div>\n                    </div>\n                    <div class="taskStatus" *ngIf="task.status !== \'Complete\'">\n                        {{task.status}}\n                    </div>\n\n                    <div>\n                        <img src="assets/imgs/option1.png" class="optionImg">\n                    </div>\n                </div>\n            </ion-card-content>\n        </div>\n    </ion-card>\n    <div *ngIf="tasks == undefined " class="noRecord" text-center>\n        <div> No record Found </div>\n    </div>\n    <div *ngIf="tasks !== undefined" class="noRecord" text-center>\n        <div *ngIf="tasks.length == 0">\n            No record Found\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/task-list/task-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_popover_popover_controller__["a" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_4__providers_gateway_gateway__["a" /* GatewayProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_global_global__["a" /* GlobalProvider */]])
    ], TaskListPage);
    return TaskListPage;
}());

//# sourceMappingURL=task-list.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_gateway_gateway__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TaskDetailsPage = /** @class */ (function () {
    function TaskDetailsPage(navCtrl, popoverCtrl, navParams, global, viewCtrl, gateway) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.navParams = navParams;
        this.global = global;
        this.viewCtrl = viewCtrl;
        this.gateway = gateway;
        this.taskSeg = "people";
        this.task = null;
        this.statusName = "";
        this.tasks = [];
        this.message = "";
        this.commentData = [];
        this.task = this.navParams.get("data");
        if (this.task !== undefined) {
            this.statusName = this.task.status;
        }
        console.log(this.navParams.get("data"), "this.taskDetail");
    }
    TaskDetailsPage.prototype.ionViewDidEnter = function () {
        this.task = this.navParams.get("data");
        if (this.task !== undefined) {
            this.statusName = this.task.status;
            console.log(this.statusName, "this.statusName");
        }
        this.getAllViewComment();
    };
    TaskDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TaskDetailsPage');
    };
    TaskDetailsPage.prototype.approvedStatus = function () {
        var self = this;
        var apprvedData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: "Complete",
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id,
            creatorChoice: "Approved"
        };
        self.gateway.updateCreatorStatus(apprvedData).then(function (result) {
            if (result.success) {
                console.log("creatorstatus", result);
                self.getAllTasks();
            }
        });
    };
    TaskDetailsPage.prototype.rejectedStatus = function () {
        var _this = this;
        var self = this;
        var rejectedData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: "Complete",
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id,
            creatorChoice: "Rejected"
        };
        self.gateway.updateCreatorStatus(rejectedData).then(function (result) {
            if (result.success) {
                console.log("creatorstatus", result);
                self.getAllTasks();
                _this.statusName = "In-progress";
            }
        });
    };
    TaskDetailsPage.prototype.changeStatus = function () {
    };
    TaskDetailsPage.prototype.updateTaskStatus = function () {
        var _this = this;
        var self = this;
        var requestData = {
            assigned_to: [],
            task_id: self.task._id,
            taskStatus: self.statusName,
            process_id: self.task.project_stage_process_id,
            activiy_id: self.task.project_activity_id,
            stage_id: self.task.project_stage_id,
            project_id: self.task.project_id
        };
        self.gateway.updateTaskStatus(requestData).then(function (result) {
            if (result.success) {
                _this.getAllTasks();
            }
        });
    };
    TaskDetailsPage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.setBackButtonText('');
    };
    TaskDetailsPage.prototype.commentViewApi = function () {
        var self = this;
        var commentData = {
            commentable_type: "task",
            commentable_id: this.task._id
        };
        self.gateway.commentViewApi(commentData).then(function (result) {
            if (result.success) {
                console.log("commentViewApi", result);
                self.getAllTasks();
            }
        });
    };
    TaskDetailsPage.prototype.getAllTasks = function () {
        var _this = this;
        var self = this;
        var taskData = this.global.taskData;
        self.gateway.getAllTaskDetails(taskData).then(function (result) {
            console.log("taskData", result);
            if (result.length > 0) {
                self.tasks = result.filter(function (par, index) {
                    return index === result.findIndex(function (obj) {
                        return JSON.stringify(obj) === JSON.stringify(par);
                    });
                });
                var taskData_1 = self.tasks.filter(function (procData) { return procData._id == self.task._id; });
                _this.task = taskData_1[0];
                console.log(_this.task, "this.task");
            }
        });
    };
    TaskDetailsPage.prototype.getAllViewComment = function () {
        var _this = this;
        var self = this;
        var commentViewData = {
            commentable_type: "task",
            commentable_id: self.task.id
        };
        self.gateway.commentViewApi(commentViewData).then(function (result) {
            console.log("commentViewApi", result);
            _this.commentData = _this.sortData(result);
        });
    };
    TaskDetailsPage.prototype.sortData = function (result) {
        console.log("asdasd", result);
        return result.sort(function (a, b) {
            if (a.createdAt > b.createdAt) {
                return 1;
            }
            else if (a.createdAt < b.createdAt) {
                return -1;
            }
            else {
                return 0;
            }
        });
        ;
    };
    TaskDetailsPage.prototype.sendMessage = function () {
        var self = this;
        if (self.message !== "") {
            var messageData = {
                commentable_type: "task",
                commentable_id: self.task._id,
                comment_message: self.message,
                process_id: self.task.project_stage_process_id,
                task_id: self.task._id,
                activiy_id: self.task.project_activity_id,
                stage_id: self.task.project_stage_id,
                project_id: self.task.project_id
            };
            self.gateway.sendMessage(messageData).then(function (result) {
                self.message = "";
                self.getAllViewComment();
            });
        }
    };
    TaskDetailsPage.prototype.calcuateDif = function (date) {
        var diff = Math.abs(new Date().getTime() - new Date(date).getTime());
        var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        return diffDays;
    };
    TaskDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-task-details',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/task-details/task-details.html"*/'<ion-header text-center>\n    <ion-navbar>\n        <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>\n            <img src="assets/imgs/logo.png" height="38">\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <div class="taskTitle">\n                    <span class="uniqueTask">{{ task.task_unique_no }} -</span>\n                    <span class="taskSubject">{{task.subject}}</span>\n                </div>\n            </ion-col>\n        </ion-row>\n\n        <ion-row>\n            <ion-col col-6>\n                <div class="dueDate" [ngClass]="{\'low-class\': task.priority ==\'low\', \'high-class\': task.priority ==\'high\', \'medium-class\': task.priority== \'medium\' }">\n\n                    {{task.due_date | date: \'d MMMM y\'}}\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div *ngIf="task.creator_status !== undefined && task.status == \'Complete\'" style="float:right;">\n                    <button class="approvedBtn" [disabled]="task.creator_status == \'Approved\'" (click)="approvedStatus()" [ngClass]="{\'accBtn\': task.creator_status == \'Approved\'}">\n                        Approved\n                    </button>\n                    <button class="rejectedBtn" [disabled]="task.creator_status == \'Rejected\'" (click)="rejectedStatus()" [ngClass]="{\'rejBtn\': task.creator_status == \'Rejected\'}">\n                        Rejected\n                    </button>\n                </div>\n                <div *ngIf="task.creator_status == undefined && task.status == \'Complete\'" style="float:right;">\n                    <button class="approvedBtn" (click)="approvedStatus()">\n                        Approved\n                    </button>\n                    <button class="rejectedBtn" (click)="rejectedStatus()">\n                        Rejected\n                    </button>\n                </div>\n                <div *ngIf="task.status !== \'Complete\'">\n                    <button class="defaultButton">\n                        {{task.status}}\n                    </button>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div class="description">\n                    {{task.description}}\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-8>\n                <ion-item>\n                    <ion-label>Status</ion-label>\n                    <ion-select multiple="false" [(ngModel)]="statusName" (ionChange)="changeStatus()" style="width:100%; max-width:100%">\n                        <ion-option value="Draft">Draft</ion-option>\n                        <ion-option value="In-progress">In-progress</ion-option>\n                        <ion-option value="Question">Question</ion-option>\n                        <ion-option value="Complete">Complete</ion-option>\n                    </ion-select>\n                </ion-item>\n            </ion-col>\n            <ion-col col-4>\n                <button class="submitBtn" (click)="updateTaskStatus()">\n                    Submit\n                </button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <div class="segmentButton">\n        <ion-segment [(ngModel)]="taskSeg" color="primary">\n            <ion-segment-button value="people">\n                Peoples\n            </ion-segment-button>\n            <ion-segment-button value="files">\n                Files\n            </ion-segment-button>\n            <ion-segment-button value="discussion">\n                Discussion\n            </ion-segment-button>\n\n        </ion-segment>\n    </div>\n    <div *ngIf="taskSeg == \'people\'">\n        <ion-card>\n            <ion-card-header>\n                <div class="accountable">\n                    Accountable\n                </div>\n            </ion-card-header>\n            <ion-card-content>\n                <div *ngFor="let creator of task.creator" class="creator">\n                    <div class="userImage">\n                        <span *ngIf="creator.profile_image">\n                            <img src="{{global.liveImgUrl + \'/\'+ creator.id + \'/\' + creator.profile_image}}" />\n                        </span>\n                        <span *ngIf="creator.profile_image == null">\n                            <img src="{{global.defaultImgUrl}}" />\n                        </span>\n                    </div>\n                    <div class="userInfo">\n                        <div class="userName">\n                            {{creator.name}}\n                        </div>\n\n                        <div class="company">\n                            Company: {{creator.company ? creator.company : \'NA\'}}\n                        </div>\n                        <div class="role">\n                            Role: {{creator.role ? creator.role : \'NA\'}}\n                        </div>\n                    </div>\n                </div>\n            </ion-card-content>\n        </ion-card>\n        <ion-card>\n            <ion-card-header>\n                <div class="responsible">\n                    Responsible\n                </div>\n            </ion-card-header>\n            <ion-card-content>\n                <div *ngFor="let assign of task.assigned_to" class="assignp">\n                    <div class="userImage">\n                        <span *ngIf="assign.profile_image">\n                            <img src="{{global.liveImgUrl + \'/\'+ assign.id + \'/\' + assign.profile_image}}" />\n                        </span>\n                        <span *ngIf="assign.profile_image == null">\n                            <img src="{{global.defaultImgUrl}}" />\n                        </span>\n                    </div>\n                    <div class="userInfo">\n                        <div class="userName">\n                            {{assign.name}}\n                        </div>\n\n                        <div class="company">\n                            Company: {{assign.company ? assign.company : \'NA\'}}\n                        </div>\n                        <div class="role">\n                            Role: {{assign.role ? assign.role : \'NA\'}}\n                        </div>\n                    </div>\n                </div>\n            </ion-card-content>\n        </ion-card>\n    </div>\n    <div *ngIf="taskSeg == \'files\'">\n        <ion-card *ngIf="task.files.length > 0">\n            <ion-card-content>\n                <div *ngFor="let taskFile of task.files">\n                    <div class="taskFileDetail">\n                        <ion-icon name="document"></ion-icon> &nbsp;\n                        <div>{{taskFile.file_name}} </div>\n                    </div>\n\n                </div>\n            </ion-card-content>\n        </ion-card>\n\n    </div>\n    <div *ngIf="taskSeg == \'discussion\'">\n        <ion-card>\n            <ion-card-content>\n                <ion-scroll scrollY="true">\n                    <div *ngFor="let comment of commentData">\n                        <div class="userImage commentImage">\n                            <span *ngIf="comment.profile.profile_image">\n                                <img src="{{global.liveImgUrl + \'/\'+ comment.profile.id + \'/\' + comment.profile.profile_image}}" />\n                            </span>\n                            <span *ngIf="comment.profile.profile_image == null">\n                                <img src="{{global.defaultImgUrl}}" />\n                            </span>\n                        </div>\n                        <div class="userInfo commentInfo">\n                            <div class="userName">\n                                {{comment.profile.first_name}}\n                                <span class="commentDays">{{ comment.createdAt | timeAgo}}</span>\n                            </div>\n\n                            <div class="company">\n                                {{comment.comment_message}}\n                            </div>\n                        </div>\n                    </div>\n                </ion-scroll>\n\n\n            </ion-card-content>\n        </ion-card>\n\n    </div>\n\n</ion-content>\n<ion-footer *ngIf="taskSeg == \'discussion\'">\n    <ion-grid>\n        <ion-row>\n            <ion-col col-9 class="messageInput">\n                <ion-item>\n                    <ion-input type="text" [(ngModel)]="message" name="title"></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col col-3>\n                <button class="messageBtn" (click)="sendMessage()">Submit</button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-footer>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/task-details/task-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_gateway_gateway__["a" /* GatewayProvider */]])
    ], TaskDetailsPage);
    return TaskDetailsPage;
}());

//# sourceMappingURL=task-details.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TaskPopoverPage = /** @class */ (function () {
    function TaskPopoverPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    TaskPopoverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TaskPopoverPage');
    };
    TaskPopoverPage.prototype.close = function (data) {
        this.viewCtrl.dismiss(data);
    };
    TaskPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-task-popover',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/task-popover/task-popover.html"*/'<div class="sortingList">\n    <div (click)="close(\'subject\')">\n        Subject\n    </div>\n    <div (click)="close(\'due_date\')">\n        Due Date\n    </div>\n    <div (click)="close(\'priority\')">\n        Priority\n    </div>\n    <div (click)="close(\'status\')">\n        Status\n    </div>\n    <div (click)="close(\'task_type\')">\n        Task Type\n    </div>\n    <div (click)="close(\'task_unique_no\')">\n        Task Unique No\n    </div>\n</div>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/task-popover/task-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */]])
    ], TaskPopoverPage);
    return TaskPopoverPage;
}());

//# sourceMappingURL=task-popover.js.map

/***/ }),

/***/ 121:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 121;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		279,
		4
	],
	"../pages/stage/stage.module": [
		280,
		3
	],
	"../pages/task-details/task-details.module": [
		281,
		2
	],
	"../pages/task-list/task-list.module": [
		282,
		1
	],
	"../pages/task-popover/task-popover.module": [
		283,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(228);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_gateway_gateway__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_global_global__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_stage_stage__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_task_list_task_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_task_details_task_details__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_task_popover_task_popover__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_time_ago_pipe__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_fcm__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_stage_stage__["a" /* StagePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_task_list_task_list__["a" /* TaskListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_task_details_task_details__["a" /* TaskDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_task_popover_task_popover__["a" /* TaskPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_15_time_ago_pipe__["a" /* TimeAgoPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    mode: 'ios',
                    menuType: 'overlay',
                }, {
                    links: [
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stage/stage.module#StagePageModule', name: 'StagePage', segment: 'stage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task-details/task-details.module#TaskDetailsPageModule', name: 'TaskDetailsPage', segment: 'task-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task-list/task-list.module#TaskListPageModule', name: 'TaskListPage', segment: 'task-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task-popover/task-popover.module#TaskPopoverPageModule', name: 'TaskPopoverPage', segment: 'task-popover', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_stage_stage__["a" /* StagePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_task_list_task_list__["a" /* TaskListPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_task_details_task_details__["a" /* TaskDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_task_popover_task_popover__["a" /* TaskPopoverPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_gateway_gateway__["a" /* GatewayProvider */],
                __WEBPACK_IMPORTED_MODULE_9__providers_global_global__["a" /* GlobalProvider */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_fcm__["a" /* FCM */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_fcm__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, fcm, statusBar, splashScreen) {
        this.platform = platform;
        this.fcm = fcm;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        if (this.platform.is('ios') || this.platform.is('android')) {
            this.fcm.getToken().then(function (token) {
                console.log("token", token);
            });
            this.fcm.onNotification().subscribe(function (data) {
                if (data.wasTapped) {
                    console.log("Received in background");
                }
                else {
                    console.log("Received in foreground");
                }
                ;
            });
            this.fcm.onTokenRefresh().subscribe(function (token) {
                console.log("token", token);
            });
        }
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Log out', component: __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/app/app.html"*/'<ion-menu [content]="content" persistent="true" side="right">\n    <ion-header>\n        <ion-toolbar>\n            <ion-title>Menu</ion-title>\n        </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n        <ion-list>\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n                {{p.title}}\n            </button>\n        </ion-list>\n    </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_fcm__["a" /* FCM */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GatewayProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_global__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GatewayProvider = /** @class */ (function () {
    function GatewayProvider(http, global, loadingCtrl) {
        this.http = http;
        this.global = global;
        this.loadingCtrl = loadingCtrl;
        this.loading = null;
        console.log('Hello GatewayProvider Provider');
    }
    GatewayProvider.prototype.httpPost = function (reqBody, reqUrl) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            self.showLoading();
            self.http
                .post(reqUrl, reqBody, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                self.hideLoading();
                resolve(response);
            });
        }).catch(function (error) {
            console.log(error);
        });
    };
    ;
    GatewayProvider.prototype.getDashboard = function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .get(self.global.baseUrl + "dashboard", { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                self.hideLoading();
                resolve(response);
            });
        });
    };
    ;
    GatewayProvider.prototype.hideLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
        }
    };
    GatewayProvider.prototype.login = function (userData) {
        var self = this;
        var url = self.global.baseUrl + "authenticate";
        return new Promise(function (resolve, reject) {
            self
                .httpPost(userData, url)
                .then(function (data) {
                resolve(data);
            })
                .catch(function (error) {
                console.log(error);
            });
        });
    };
    GatewayProvider.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: '',
        });
        this.loading.present();
    };
    GatewayProvider.prototype.getStageDetails = function (projectId) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .get(self.global.baseUrl + "projects/" + projectId + "/details", { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                self.hideLoading();
                resolve(response);
            });
        });
    };
    ;
    GatewayProvider.prototype.getAllTaskDetails = function (reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonPostApi("tasks/", reqData).then(function (result) {
                resolve(result);
            });
        });
    };
    GatewayProvider.prototype.commonPostApi = function (url, body) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            self.showLoading();
            self.http
                .post(self.global.baseUrl + url, body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                self.hideLoading();
                resolve(response);
            });
        });
    };
    GatewayProvider.prototype.commonTokenPostApi = function (url, body) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Content-Type", "application/json");
            headers.append("Accept", "application/json");
            headers.set("token", window.localStorage.getItem("token"));
            self.showLoading();
            self.http
                .post(self.global.baseUrl + url, body, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                self.hideLoading();
                resolve(response);
            });
        });
    };
    GatewayProvider.prototype.updateTaskStatus = function (reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("tasks/updateUserTaskStatus/", reqData).then(function (result) {
                resolve(result);
            });
        });
    };
    GatewayProvider.prototype.commentViewApi = function (reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("comment/view", reqData).then(function (result) {
                resolve(result);
            });
        });
    };
    GatewayProvider.prototype.updateCreatorStatus = function (reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("tasks/updateCreatorStatus", reqData).then(function (result) {
                resolve(result);
            });
        });
    };
    GatewayProvider.prototype.sendMessage = function (reqData) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.commonTokenPostApi("comment/store", reqData).then(function (result) {
                resolve(result);
            });
        });
    };
    GatewayProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], GatewayProvider);
    return GatewayProvider;
}());

//# sourceMappingURL=gateway.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GlobalProvider = /** @class */ (function () {
    function GlobalProvider(http) {
        this.http = http;
        this.isDev = false;
        this.isLive = true;
        this.baseUrl = this.getBaseURl();
        this.token = "";
        this.taskData = {};
        this.allTasks = [];
        this.liveImgUrl = "https://s3-ap-northeast-1.amazonaws.com/consio-user-profile-images";
        this.defaultImgUrl = "https://consio.app/assets/images/profil_page/frienddefault.jpg";
        console.log('Hello GlobalProvider Provider');
    }
    GlobalProvider.prototype.getBaseURl = function () {
        if (this.isDev) {
            return "http://localhost:3443/oxelus/api/v1/";
        }
        else {
            return "https://consio.app:3443/oxelus/api/v1/";
        }
    };
    GlobalProvider.prototype.filterItems = function (searchTerm) {
        if (this.allTasks.length > 0) {
            return this.allTasks.filter(function (item) {
                if (item.subject.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.status.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.creator_status !== undefined) {
                    if (item.creator_status.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                        return true;
                    }
                }
            });
        }
    };
    GlobalProvider.prototype.filterData = function (stringData) {
        console.log(stringData);
        console.log("alltasks", this.allTasks);
        return this.allTasks.sort(function (a, b) {
            if (a[stringData] < b[stringData]) {
                return -1;
            }
            else if (a[stringData] > b[stringData]) {
                return 1;
            }
            else {
                return 0;
            }
        });
    };
    GlobalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], GlobalProvider);
    return GlobalProvider;
}());

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_gateway_gateway__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__stage_stage__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, gateway) {
        this.navCtrl = navCtrl;
        this.gateway = gateway;
        this.projects = [];
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var self = this;
        this.gateway.getDashboard().then(function (result) {
            if (result.success) {
                self.projects = result.projects;
                console.log("self.projects", self.projects);
            }
        });
    };
    ;
    HomePage.prototype.goToStages = function (projectDetail) {
        var self = this;
        self.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__stage_stage__["a" /* StagePage */], {
            projectDetail: projectDetail
        });
    };
    ;
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/admin/Documents/consio-demo/consio/src/pages/home/home.html"*/'<ion-header text-center>\n    <ion-navbar>\n        <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>\n            <img src="assets/imgs/logo.png" height="38">\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-list>\n        <ion-item *ngFor="let projectDetail of projects" (click)="goToStages(projectDetail)" deatil-push>\n            <div *ngIf="projects.length > 0">\n                <h2>{{projectDetail.project.name}}</h2>\n                <ion-icon name="arrow-forward" class="rightIcon"></ion-icon>\n                <p>\n                    {{projectDetail.role.name}}\n                </p>\n            </div>\n            <div *ngIf="projectDetail.length == 0" text-center> No record Found</div>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/admin/Documents/consio-demo/consio/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_gateway_gateway__["a" /* GatewayProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map